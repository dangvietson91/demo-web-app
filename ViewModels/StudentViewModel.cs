﻿using System;

namespace DemoWebApp.ViewModels
{
    public class StudentViewModel
    {
        public int StudentId { get; set; }
        public string FullName { get; set; }
        public DateTime DoB { get; set; }
    }
}