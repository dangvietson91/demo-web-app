﻿using System.Collections.Generic;

namespace DemoWebApp.ViewModels
{
    public class QueryTeacherStudentViewModel
    {
        public QueryTeacherStudentViewModel()
        {
            Teachers = new List<TeacherViewModel>();
        }

        public List<TeacherViewModel> Teachers { get; set; }
    }
}