﻿using DemoWebApp.Models;
using DemoWebApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DemoWebApp.Controllers
{
    public class DemoController : Controller
    {
        private DemoWebDbContext db = new DemoWebDbContext();

        public ActionResult Index()
        {
            QueryTeacherStudentViewModel model = new QueryTeacherStudentViewModel();

            #region Query Student & Teacher List
            List<Teacher> teachers = db.Teachers.ToList();
            List<Student> students = db.Students.ToList();
            #endregion

            #region Mapping View Model
            foreach (var teacher in teachers)
            {
                TeacherViewModel teacherViewModel = new TeacherViewModel();
                teacherViewModel.TeacherId = teacher.TeacherId;
                teacherViewModel.FullName = teacher.FullName;

                foreach (var student in students)
                {
                    if (student.TeacherId == teacher.TeacherId)
                    {
                        StudentViewModel studentViewModel = new StudentViewModel();
                        studentViewModel.StudentId = student.StudentId;
                        studentViewModel.FullName = student.FullName;
                        studentViewModel.DoB = student.DoB;
                        teacherViewModel.Students.Add(studentViewModel);
                    }
                }

                model.Teachers.Add(teacherViewModel);
            }
            #endregion

            #region Order Name Teacher & DoB Student
            model.Teachers = model.Teachers.OrderBy(x => x.FullName).ToList();

            foreach (var teacher in model.Teachers)
            {
                teacher.Students.OrderBy(x => x.DoB).ToList();
            }
            #endregion

            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(List<CreateStudentTeacherViewModel> createModel)
        {
            List<Teacher> teachers = db.Teachers.ToList();

            if (createModel != null && createModel.Count >= 30)
            {
                #region Mapping Models
                foreach (var model in createModel)
                {
                    int teacherId;

                    #region Handle New Teacher (Example: Duplicate values are not allowed)
                    if (!teachers.Any(x => x.FullName == model.TeacherName))
                    {
                        Teacher newTeacher = new Teacher()
                        {
                            FullName = model.TeacherName
                        };

                        db.Teachers.Add(newTeacher);
                        db.SaveChanges();
                        teacherId = newTeacher.TeacherId;
                    }
                    else
                    {
                        teacherId = teachers.FirstOrDefault(x => x.FullName == model.TeacherName).TeacherId;
                    }
                    #endregion

                    #region Handle Student (Example: Duplicate values are allowed)
                    Student newStudent = new Student()
                    {
                        FullName = model.FullName,
                        DoB = model.DoB,
                        TeacherId = teacherId
                    };

                    db.Students.Add(newStudent);
                    db.SaveChanges();
                    #endregion
                }
                #endregion
            }

            return View();
        }
    }
}