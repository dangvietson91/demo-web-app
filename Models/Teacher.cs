﻿using System.ComponentModel;

namespace DemoWebApp.Models
{
    public class Teacher
    {
        [DisplayName("Teacher Code")]
        public int TeacherId { get; set; }
        [DisplayName("Teacher Name")]
        public string FullName { get; set; }
    }
}