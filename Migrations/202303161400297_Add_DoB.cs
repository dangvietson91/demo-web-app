namespace DemoWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_DoB : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "DoB", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Students", "DoB");
        }
    }
}
