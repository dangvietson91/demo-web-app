﻿using System;
using System.ComponentModel;

namespace DemoWebApp.Models
{
    public class Student
    {
        [DisplayName("Student Code")]
        public int StudentId { get; set; }
        [DisplayName("Student Name")]
        public string FullName { get; set; }
        [DisplayName("DoB")]
        public DateTime DoB { get; set; }
        [DisplayName("Teacher Code")]
        public int TeacherId { get; set; }
    }
}