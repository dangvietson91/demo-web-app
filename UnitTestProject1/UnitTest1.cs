﻿using DemoWebApp.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestOrderByFullNameAndDoB()
        {
            // Arrange
            var model = new QueryTeacherStudentViewModel
            {
                Teachers = new List<TeacherViewModel>
            {
                new TeacherViewModel { FullName = "John Smith", Students = new List<StudentViewModel> { new StudentViewModel { DoB = new DateTime(2005, 6, 1) }, new StudentViewModel { DoB = new DateTime(2006, 5, 1) } } },
                new TeacherViewModel { FullName = "Jane Doe", Students = new List<StudentViewModel> { new StudentViewModel { DoB = new DateTime(2004, 4, 1) }, new StudentViewModel { DoB = new DateTime(2007, 3, 1) } } },
                new TeacherViewModel { FullName = "Alice Brown", Students = new List<StudentViewModel> { new StudentViewModel { DoB = new DateTime(2003, 2, 1) }, new StudentViewModel { DoB = new DateTime(2008, 1, 1) } } },
            }
            };

            // Act
            model.Teachers = model.Teachers.OrderBy(x => x.FullName).ToList();
            foreach (var teacher in model.Teachers)
            {
                teacher.Students.OrderBy(x => x.DoB).ToList();
            }

            // Assert
            NUnit.Framework.Assert.AreEqual("Alice Brown", model.Teachers[0].FullName);
            NUnit.Framework.Assert.AreEqual(new DateTime(2003, 2, 1), model.Teachers[0].Students[0].DoB);
            NUnit.Framework.Assert.AreEqual(new DateTime(2008, 1, 1), model.Teachers[0].Students[1].DoB);

            NUnit.Framework.Assert.AreEqual("Jane Doe", model.Teachers[1].FullName);
            NUnit.Framework.Assert.AreEqual(new DateTime(2004, 4, 1), model.Teachers[1].Students[0].DoB);
            NUnit.Framework.Assert.AreEqual(new DateTime(2007, 3, 1), model.Teachers[1].Students[1].DoB);

            NUnit.Framework.Assert.AreEqual("John Smith", model.Teachers[2].FullName);
            NUnit.Framework.Assert.AreEqual(new DateTime(2005, 6, 1), model.Teachers[2].Students[0].DoB);
            NUnit.Framework.Assert.AreEqual(new DateTime(2006, 5, 1), model.Teachers[2].Students[1].DoB);
        }
    }
}
