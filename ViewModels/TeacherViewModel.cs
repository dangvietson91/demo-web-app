﻿using System.Collections.Generic;

namespace DemoWebApp.ViewModels
{
    public class TeacherViewModel
    {
        public TeacherViewModel()
        {
            Students = new List<StudentViewModel>();
        }

        public int TeacherId { get; set; }
        public string FullName { get; set; }
        public List<StudentViewModel> Students { get; set; }
    }
}