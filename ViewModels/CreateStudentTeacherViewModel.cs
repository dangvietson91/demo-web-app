﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DemoWebApp.ViewModels
{
    public class CreateStudentTeacherViewModel
    {
        public string FullName { get; set; }
        [DisplayFormat(DataFormatString = "{dd-mm-yyyy}")]
        public DateTime DoB { get; set; }
        public string TeacherName { get; set; }
    }
}